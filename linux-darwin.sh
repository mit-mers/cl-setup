#!/usr/bin/bash

###
# https://gitlab.com/mit-mers/cl-setup
#
# Interactively call with: curl -s https://gitlab.com/mit-mers/cl-setup/-/raw/main/linux-darwin.sh | bash
#
# Command-line arguments:
#     -y Yes to all prompts
#     -s Silence warnings
#     -h help
###

usage() { echo "Usage: $0 [-y yes to all] [-s silence] [-h help]" 1>&2; exit 0; }

YES_TO_ALL=false
SILENCE=false

while getopts ysh flag; do
    case "${flag}" in
        y) YES_TO_ALL=true;;
        s) SILENCE=true;;
        h) usage;;
    esac
done

YELLOW=$(tput setaf 3)
NORMAL=$(tput sgr0)

warn_user () {
    if ! $SILENCE; then
        printf "${YELLOW}$1${NORMAL}\n"
    fi
}

declare -a ACTIONS_PERFORMED
note_action () {
    ACTIONS_PERFORMED+=( "$*" )
}

should_continue () {
    $YES_TO_ALL && return 0
    warn_user "Is this ok? y/[n]"
    read yn < /dev/tty
    [ $yn == "y" ]
}

if ! which sbcl &> /dev/null; then
   warn_user "You must install sbcl first. We recommend using your OS package manager"
   exit 1
fi

if ! which git &> /dev/null; then
   warn_user "You must install git first. We recommend using your OS package manager"
   exit 1
fi

if ! which gpg &> /dev/null; then
    warn_user "Cannot find gpg, which we use to verify the installation of clpm. We will continue anyway"
    if ! should_continue; then
        exit 1
    fi
fi

set -e

ASDF_VERSION=3.3.5.3
EXISTING_ASDF=""
ASDF_CLONED=false
if [ -d ~/.local/share/common-lisp/source/asdf ]; then
    # they already cloned ASDF. check its version
    ASDF_CLONED=true
    EXISTING_ASDF=$(sbcl \
        --noinform --non-interactive --no-userinit --no-sysinit \
        --load ~/.local/share/common-lisp/source/asdf/build/asdf.lisp \
        --eval "(print (asdf:asdf-version))" | xargs | tr -d ' "')

fi

SHOULD_INSTALL_ASDF=false
if [ "$EXISTING_ASDF" != "$ASDF_VERSION" ]; then
    SHOULD_INSTALL_ASDF=true
else
    echo "ASDF is already at v${ASDF_VERSION}"
fi

if $SHOULD_INSTALL_ASDF; then
    echo "Updating ASDF to v${ASDF_VERSION}..."

    mkdir -p ~/.local/share/common-lisp/source > /dev/null
    if ! $ASDF_CLONED; then
        git clone --quiet https://gitlab.common-lisp.net/asdf/asdf ~/.local/share/common-lisp/source/asdf > /dev/null
        note_action "Cloned ASDF to ~/.local/share/common-lisp/source/asdf"
    fi

    pushd ~/.local/share/common-lisp/source/asdf > /dev/null
    git pull origin master --quiet
    make > /dev/null
    popd > /dev/null

    # test that asdf was loaded
    sbcl \
        --non-interactive --no-userinit --no-sysinit \
        --load ~/.local/share/common-lisp/source/asdf/build/asdf.lisp \
        --eval "(print (asdf:asdf-version))" > /dev/null
    echo "...done"
fi

SHOULD_UPDATE_SBCLRC=true
if [[ -f ~/.sbclrc ]]; then
    warn_user "Existing ~/.sbclrc found. We will back it up and create a new ~/.sbclrc"
    # if we updated ASDF, we must change ~/.sbclrc accordingly to load asdf.lisp
    if ! $SHOULD_UPDATE_ASDF && ! should_continue; then
        SHOULD_UPDATE_SBCLRC=false
    else
        mv ~/.sbclrc ~/.sbclrc.backup
        note_action "Moved existing ~/.sbclrc to ~/.sbclrc.backup"
    fi
fi

# if you want to make sure this version is the latest, see https://files.clpm.dev/clpm/index.html
CLPM_VERSION=0.4.1
INSTALLED_CLPM=false
SHOULD_INSTALL_CLPM=true

if which clpm &> /dev/null; then
    EXISTING_CLPM_VERSION=$(clpm version)
    if ! [ $EXISTING_CLPM_VERSION = $CLPM_VERSION ]; then
        warn_user "CLPM version ${EXISTING_CLPM_VERSION} is installed, but ${CLPM_VERSION} is available. We want to install it for you"
        if ! should_continue; then
            SHOULD_INSTALL_CLPM=false
        fi
    else
        SHOULD_INSTALL_CLPM=false
        echo "CLPM is already at v${CLPM_VERSION}"
    fi
fi

if $SHOULD_INSTALL_CLPM; then
    echo "Downloading CLPM v${CLPM_VERSION} and verifying integrity..."
    OS_SUFFIX="linux"
    ARCH_SUFFIX="amd64"

    if [[ ! $(uname -m) = "x86_64"* ]]; then
        ARCH_SUFFIX="arm64"
    fi

    UNAME=$(uname -s | tr '[:upper:]' '[:lower:]')
    if [[ $UNAME == darwin ]]; then
        OS_SUFFIX="darwin"
    fi
    CLPM_COMPOSITE=clpm-${CLPM_VERSION}-${OS_SUFFIX}-${ARCH_SUFFIX}

    curl -sOL https://files.clpm.dev/clpm/${CLPM_COMPOSITE}.tar.gz

    if which gpg &> /dev/null; then
        CLPM_KEY="10327DE761AB977333B1AD7629932AC49F3044CE"
        curl -sOL https://files.clpm.dev/clpm/clpm-${CLPM_VERSION}.DIGESTS.asc
        curl -sOL https://files.clpm.dev/clpm/clpm-signing-key.asc

        gpg --batch --keyserver keyserver.ubuntu.com --recv-keys ${CLPM_KEY} \
            || gpg --batch --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys ${CLPM_KEY} \
            || gpg --batch --keyserver pgp.mit.edu --recv-keys ${CLPM_KEY}

        gpg --batch -d clpm-${CLPM_VERSION}.DIGESTS.asc > clpm-${CLPM_VERSION}.DIGESTS.tmp
        grep -E -- ${CLPM_COMPOSITE}.tar.gz clpm-${CLPM_VERSION}.DIGESTS.tmp > clpm-${CLPM_VERSION}.DIGESTS

        if [[ $UNAME == darwin ]]; then
            # mac does not have sha512sum
            shasum -a 512 -c clpm-${CLPM_VERSION}.DIGESTS
        else
            sha512sum -c clpm-${CLPM_VERSION}.DIGESTS
        fi

        rm clpm-${CLPM_VERSION}.DIGESTS*
    fi

    echo "...done"

    echo "Installing CLPM..."
    tar xzf ${CLPM_COMPOSITE}.tar.gz > /dev/null
    pushd ${CLPM_COMPOSITE} &> /dev/null

    if which clpm &> /dev/null; then
        # get rid of an older version of CLPM if it already exists
        sudo rm -rf $(which clpm)
    fi

    sudo sh install.sh > /dev/null
    popd &> /dev/null
    rm -rf ${CLPM_COMPOSITE} ${CLPM_COMPOSITE}.tar.gz clpm-signing-key.asc

    echo "...done"
fi

echo "Configuring CLPM..."

mkdir -p ~/.config/clpm ~/.config/common-lisp/source-registry.conf.d > /dev/null

SHOULD_UPDATE_SOURCE_REGISTRY=true
if [[ -f ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf ]]; then
    warn_user "Existing ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf file found. We want to back it up and create a new source registry"
    if ! should_continue; then
        SHOULD_UPDATE_SOURCE_REGISTRY=false
    else
        mv ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf.backup
        note_action "Moved existing ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf to ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf.backup"
    fi
fi

clpm client install > /dev/null
if $SHOULD_UPDATE_SOURCE_REGISTRY; then
    clpm client source-registry.d > ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf
    note_action "Created ~/.config/common-lisp/source-registry.conf.d/20-clpm-client.conf"
fi

SHOULD_UPDATE_SOURCES=true
if [[ -f ~/.config/clpm/sources.conf ]]; then
    warn_user "Existing ~/.config/clpm/sources.conf file found. We want to back it up and create a new sources.conf"
    if ! should_continue; then
        SHOULD_UPDATE_SOURCES=false
    else
        mv ~/.config/clpm/sources.conf ~/.config/clpm/sources.conf.backup
        warn_user "Moved existing ~/.config/clpm/sources.conf to ~/.config/clpm/sources.conf.backup"
    fi
fi

if $SHOULD_UPDATE_SOURCES; then
    echo ';;; -*- mode: common-lisp; -*-
;; MERS systems. You must have git access to MERS repos to access
("mers"
  :type :clpi
  :url "https://clpi.mers.group/")

;; all other systems
("quicklisp"
  :type :quicklisp
  :url "https://beta.quicklisp.org/dist/quicklisp.txt")' > ~/.config/clpm/sources.conf
    note_action "Created ~/.config/clpm/sources.conf"
fi

if $SHOULD_UPDATE_SBCLRC; then
    echo ";;; Use CLPM with custom configuration
;;;
;;; Generated by https://gitlab.com/mit-mers/lisp-onboarding
;;; Modified from \`clpm client rc\` from CLPM v${CLPM_VERSION}

;; The difference between this and the default CLPM config is that this script
;; will load ASDF from asdf.lisp instead of (require :asdf)
(load \"${HOME}/.local/share/common-lisp/source/asdf/build/asdf.lisp\")
#-clpm-client
(when (asdf:find-system \"clpm-client\" nil)
  ;; Load the CLPM client if we can find it.
  (asdf:load-system \"clpm-client\")
  (when (uiop:symbol-call :clpm-client '#:active-context)
    ;; If started inside a context (i.e., with \`clpm exec\` or \`clpm bundle exec\`),
    ;; activate ASDF integration
    (uiop:symbol-call :clpm-client '#:activate-asdf-integration)))
">> ~/.sbclrc
fi

SHOULD_UPDATE_CLPM_CONF=true
if [[ -f ~/.config/clpm/clpm.conf ]]; then
    warn_user "Existing ~/.config/clpm/clpm.conf file found. We want to back it up and create a new clpm.conf"
    if ! should_continue; then
        SHOULD_UPDATE_CLPM_CONF=false
    else
        mv ~/.config/clpm/clpm.conf ~/.config/clpm/clpm.conf.backup
        note_action "Moved existing ~/.config/clpm/clpm.conf to ~/.config/clpm/clpm.conf.backup"
    fi
fi

if $SHOULD_UPDATE_CLPM_CONF; then
    echo ';;; -*- mode: common-lisp; -*-
;; This is the current recommended default configuration for CLPM
(version "0.2")

;; This configures how CLPM interacts with remote git servers.
((:git :remotes "git.yourserver.com")
 :type :gitlab
 ;; Uncomment if you would like to use your SSH key to access Gitlab:
 ;;
 ;; :method :ssh
 ;;
 ;; If you would like to use HTTPS, place a token as the value for :password.
 ;; Make sure to comment these lines if you would prefer SSH.
 :method :https
 :username "YOUR_GIT_USERNAME"
 :password "YOUR_HTTPS_TOKEN")

;; This configures how CLPM downloads first party release tarballs (i.e.,
;; tagged, "released" versions of code). This token can be the same as the one
;; used in the previous table.
((:http :headers "git.yourserver.com" :private-token)
 :secure-only-p t
 :value "YOUR_HTTPS_TOKEN")

;; This configures CLPM to use Dexador as its HTTP client
((:http-client)
 :type :dexador)' > ~/.config/clpm/clpm.conf
    note_action "Created ~/.config/clpm/clpm.conf"
fi

if $SHOULD_UPDATE_SBCLRC; then
    echo '(clpm-client:activate-context "default")' >> ~/.sbclrc
fi

echo "Syncing package metadata with Quicklisp... this may take a while"

sbcl --noinform --eval '(progn
  (clpm-client:sync :sources "quicklisp")
  (sb-ext:quit))' > /dev/null

echo "...done"

# no need to report any of this if the user sets -s
if $SILENCE; then
    exit 0
fi

if $SHOULD_INSTALL_ASDF || $SHOULD_INSTALL_CLPM; then
    warn_user "Here's what was installed:"
fi
if $SHOULD_INSTALL_ASDF; then
    echo ASDF v${ASDF_VERSION}
fi
if $SHOULD_INSTALL_CLPM; then
    echo CLPM v${CLPM_VERSION}
fi

if (( ${#ACTIONS_PERFORMED[@]} )); then
    warn_user "Here's a list of the files that were created and/or moved:"
    for ((i = 0; i < ${#ACTIONS_PERFORMED[@]}; i++)) do
         echo ${ACTIONS_PERFORMED[$i]}
    done
fi

warn_user "Next step for you: if you need to collect packages from any private repositories, open ~/.config/clpm/clpm.conf and edit it according to the comments"
